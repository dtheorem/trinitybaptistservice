﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrinityBaptistService.Models
{
    public class SermonInformationModel
    {
        public string Title { get; set; }
        public string Speaker { get; set; }
        public string DownloadUrl { get; set; }
        public DateTime Date { get; set; }
    }
}
