﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrinityBaptistService.Settings
{
    public static class SettingsRetriever
    {
        public static string Get(string name)
        {
            return Environment.GetEnvironmentVariable(name, EnvironmentVariableTarget.Process);
        }
    }
}
