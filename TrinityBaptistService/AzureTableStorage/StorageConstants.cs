﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrinityBaptistService.AzureTableStorage
{
    public static class StorageConstants
    {
        public const long EndOfTime = 3155063616000000000;

        public static string SermonPartition = "Sermons";
        public static string SermonTable = "Sermons";
    }
}
