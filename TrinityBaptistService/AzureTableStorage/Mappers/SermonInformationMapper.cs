﻿using System;
using System.Collections.Generic;
using System.Text;
using TrinityBaptistService.AzureTableStorage.Entities;
using TrinityBaptistService.Models;

namespace TrinityBaptistService.AzureTableStorage.Mappers
{
    public class SermonInformationMapper
    {
        public static SermonInformationEntity MapToEntity(SermonInformationModel model)
        {
            return new SermonInformationEntity
            {
                Date = model.Date,
                DownloadUrl = model.DownloadUrl,
                Speaker = model.Speaker,
                Title = model.Title,
                RowKey = MapToKey(model)
            };
        }

        public static string MapToKey(SermonInformationModel model)
        {
            return (StorageConstants.EndOfTime - model.Date.ToUniversalTime().Ticks).ToString();
        }

        internal static object MapToModel(SermonInformationEntity entity)
        {
            return new SermonInformationModel
            {
                Date = entity.Date,
                DownloadUrl = entity.DownloadUrl,
                Speaker = entity.Speaker,
                Title = entity.Title
            };
        }
    }
}
