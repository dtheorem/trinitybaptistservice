﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TrinityBaptistService.AzureTableStorage.Operators
{
    public class TableStorageGetter<T> : BaseTableOperator where T : TableEntity, new()
    {
        public TableStorageGetter(string tableName, string connectionString) 
            : base(tableName, connectionString)
        {
        }

        public async Task<List<T>> GetTop(string partitionKey, int count)
        {
            if (count > 1000)
                throw new ArgumentOutOfRangeException($"Trying to get {count} records - don't do that.");

            var query = new TableQuery<T>()
                .Where(TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, partitionKey))
                .Take(count);

            var result = await Table.ExecuteQuerySegmentedAsync(query, null);
            return result.Results;
        }
    }
}
