﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace TrinityBaptistService.AzureTableStorage.Operators
{
    public class TableStorageSetter<T> : BaseTableOperator where T : TableEntity
    {
        public TableStorageSetter(string tableName, string connectionString) 
            : base(tableName, connectionString)
        {
        }

        public async Task<bool> InsertOrReplace(T entity)
        {
            await Table.CreateIfNotExistsAsync();

            var operation = TableOperation.InsertOrReplace(entity);
            var result = await Table.ExecuteAsync(operation);

            return true;
        } 
    }
}
