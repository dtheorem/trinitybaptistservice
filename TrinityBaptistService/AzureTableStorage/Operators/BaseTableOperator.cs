﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Text;

namespace TrinityBaptistService.AzureTableStorage.Operators
{
    public abstract class BaseTableOperator
    {
        protected CloudTable Table { get; set; }

        public BaseTableOperator(string tableName, string connectionString)
        {
            var storageAccount = CloudStorageAccount.Parse(connectionString);
            var tableClient = storageAccount.CreateCloudTableClient();
            Table = tableClient.GetTableReference(tableName);
        }
    }
}
