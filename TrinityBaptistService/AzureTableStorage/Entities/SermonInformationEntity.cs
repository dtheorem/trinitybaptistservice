﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Text;

namespace TrinityBaptistService.AzureTableStorage.Entities
{
    public class SermonInformationEntity : TableEntity
    {
        public string Title { get; set; }
        public string Speaker { get; set; }
        public string DownloadUrl { get; set; }
        public DateTime Date { get; set; }

        public SermonInformationEntity()
        {
            PartitionKey = StorageConstants.SermonPartition;
        }
    }
}
