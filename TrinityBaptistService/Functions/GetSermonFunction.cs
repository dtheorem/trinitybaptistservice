
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using System.Linq;
using TrinityBaptistService.Models;
using System;
using TrinityBaptistService.AzureTableStorage.Entities;
using TrinityBaptistService.AzureTableStorage;
using Microsoft.WindowsAzure.Storage.Table;
using TrinityBaptistService.AzureTableStorage.Operators;
using TrinityBaptistService.AzureTableStorage.Mappers;
using TrinityBaptistService.Settings;

namespace TrinityBaptistService.Functions
{
    public static class GetSermonFunction
    {
        [FunctionName("GetSermonFunction")]
        public static async System.Threading.Tasks.Task<IActionResult> RunAsync(
            [HttpTrigger(AuthorizationLevel.Function, "get", Route = "getSermonInfo({numberOfLatest})")]HttpRequest req,
            TraceWriter log,
            int numberOfLatest)
        {
            if (numberOfLatest <= 0)
                return new BadRequestObjectResult("Please supply a number greater than 0.");

            var result = await
                new TableStorageGetter<SermonInformationEntity>(StorageConstants.SermonTable, SettingsRetriever.Get("ConnectionString"))
                    .GetTop(StorageConstants.SermonPartition, numberOfLatest);

            return new OkObjectResult(result.Select(x => SermonInformationMapper.MapToModel(x)));
        }
    }
}
