
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.WebJobs.Host;
using Newtonsoft.Json;
using TrinityBaptistService.Models;
using TrinityBaptistService.AzureTableStorage.Operators;
using TrinityBaptistService.AzureTableStorage;
using TrinityBaptistService.AzureTableStorage.Entities;
using TrinityBaptistService.AzureTableStorage.Mappers;
using TrinityBaptistService.Settings;

namespace TrinityBaptistService.Functions
{
    public static class UploadSermonFunction
    {
        [FunctionName("UploadSermonFunction")]
        public static async System.Threading.Tasks.Task<IActionResult> RunAsync(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "sermonInformation")]HttpRequest req,
            TraceWriter log)
        {
            string requestBody = new StreamReader(req.Body).ReadToEnd();
            var sermonInfo = JsonConvert.DeserializeObject<SermonInformationModel>(requestBody);

            if (sermonInfo == null)
                return new BadRequestObjectResult($"Error processing content body {requestBody}");

            var result = await new TableStorageSetter<SermonInformationEntity>(StorageConstants.SermonTable, SettingsRetriever.Get("ConnectionString"))
                .InsertOrReplace(SermonInformationMapper.MapToEntity(sermonInfo));

            if (result == true)
            {
                return new AcceptedResult();
            }

            return new BadRequestObjectResult("Something strange happened.");
        }
    }
}
